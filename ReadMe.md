# Introduction à 3 outils de l'écosystème
Le contenu de ce dépôt vise à présenter 3 outils à un publique néophyte :
- les notebooks (à travers *Jupyter Lab*),
- les gestionnaires de paquets et d'environnement (à travers *Pip* et *Conda*),
- les gestionnaires de version (à travers *Git*)


## Notebook_demo.ipynb
Ce notebook est utilisé comme démonstration de l'utilisation des outils (source des captures d'écran présentées dans le pdf). 
Il peut être utilisé pour une expérimentation de première main, et approfondir certains point qui ne sont pas évoqués dans la présentation.


### Requirements
L'exécution du notebook requière l'installation des librairies pointées dans requirements.txt.
Dans le contexte d'une session Vaniila vierge :
- placer les fichiers (demo_notebook.ipynb et requirements.txt) dans le répertoir de travail
- dans un terminal 
    - naviguer jusque au dossier de travail 
    - exécuter la commande `pip install -f requirements.txt`


### Licence
Les illustrations utilisées sont sous régime Creative Common ou sont tirés de la documentation de la librairie correspondante.
Les sources sont indiquées dans le lien utilisé pour le rendu markdown de la cellulle.

Shield: [![CC BY 4.0][cc-by-shield]][cc-by]
This work is licensed under a [Creative Commons Attribution 4.0 International License][cc-by].


## support_presentation.pdf
Les "diapositives" utilisées comme support lord de la présentation sont disponnibles sous forme d'un fichier pdf. Nonobstant les sections "Préambule", "Les notebooks : Besoins adressés", "Conclusion", les diapositives correspondent au contenu du notebook, repris sous forme de capture d'écran.


